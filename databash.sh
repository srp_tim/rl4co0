#!/usr/bin/env bash
#SBATCH --job-name=data#SBATCH --output=data%j.log
#SBATCH --error=data%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash

DEVICES="0"

cd rl4co
cd data

CUDA_VISIBLE_DEVICES="$DEVICES" python generate_data.py --name train_gen --problem tsp --seed 1111 --dataset_size 100000 --graph_sizes 20 50 100
python generate_data.py --name train_gen --problem tsp --seed 1111 --dataset_size 50000 --graph_sizes 200
python generate_data.py --name train_gen --problem tsp --seed 1111 --dataset_size 10000 --graph_sizes 500






